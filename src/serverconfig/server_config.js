const server = require('express')();
const bodyParser = require('body-parser');
const morgan = require('morgan');

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }))

server.use(morgan('dev'))

module.exports = server