<h1> Primeiros passos </h1>

Baixe o NodeJS e o NPM
<ul>
        <li>Sistemas Debian, Ubuntu, Linux Mint e derivados: <code>sudo apt-get install nodejs npm </code></li>
        </br>
        <li>Sistemas Windows: [Clique Aqui](https://nodejs.org/en/download/)</li>
</ul>

Clone este repositorio, depois, pelo terminal do seu SO, acesse a pasta do repositorio e digite <code>npm install</code>
<h1> Configurando a interatividade da API com o banco <h1>
<ul>
    <li>Acesse as configurações do banco em <code>/src/dataconfig/data_config.js</code>
    
          1 -  connection = mysql.createConnection({
          2 -            host: '35.196.173.240', 
          3 -            port: '3306',
          4 -            user: 'root',
          5 -            password: '251410',
          6 -            database: 'master'
               })
               
    Host; Aqui sera informado o endereço de ip do servidor caso tenha um. A caso, seja um servidor local, basta colocar localhost ou 127.0.0.1
    Port; Mantenha do jeito que esta.
    User; Informe o usuario que foi definido na criação do banco.
    Password Coloque a senha que foi definida na criação do banco.
    Database O nome do banco de dados (Schema) que sera utilizado. Por padrão, usa-se o master.
</ul>

<h1> Configurando as rotas </h1>
<ul>
    <li>Acesse as configurações do banco em <code>/index.js</code>
    O Roteamento refere-se à definição de terminais do aplicativo (URIs) e como eles respondem às solicitações do cliente. Para obter uma introdução a roteamento, consulte [Roteamento Basico](http://expressjs.com/pt-br/starter/basic-routing.html)
    
    
        1 - server.post('/cadastrar', function(req,res){
    
    Para mudar a URI da rota, basta alterar onde esta escrito /cadastrar e colocar uma de acordo com a necessidade de vcs, exemplo; /registerUser
</ul>

<h1> Definindo as informações que serão obtidas pela API e enviando a uma database </h1>
<ul>
database.query('insert into Usuarios(nome,email,login,senha) values (?,?,?,?)', [req.body.nome,req.body.email,req.body.login,req.body.senha],function(error,results,fields){
	
	Primeiro passo, é necessario informar o nome da tabela do banco dentro da query ( o comando da linha 1) que devera substituir onde esta escrito Usuarios
	    A seguir do nome da tabela, deve-se circundar o nome das colunas que serão introduzidas após uma requisição de envio para esta rota. 
	    ( É OBRIGATORIO AS COLUNAS COINCIDIR COM AS QUE ESTAO NO BANCO DE DADOS ) 
	    Precedido pela sintaxe values, sera circundado pontos de interrogações respectivos a quantidade de colunas que serão atribuidas. No caso acima, temos 4 colunas,
	         entao devemos circundar 4 pontos de interrogações como no do exemplo acima. Partindo para um conceito um pouco mais tecnico, interrogação equivale a argumentos em uma query.
        De um pulo a frente para fora da aspas simples como no caso acima e adicione um virgula.
   
    Segundo passo iremos obter as informações enviadas para o servidor durante a requisição. Para isto, devemos preceder o nome da variavel que estaremos capturando com o 'req.body',
        olhando para o exemplo acima, temos o [req.body.nome]. Vale lembrar que 'nome' é só uma variavel que iremos capturar do cabeçalho. Poderia ser muito bem batata que no caso ficaria req.body.batata.
        Vale lembrar que, não necessariamente, devemos colocar o mesmo nome da coluna do banco de dados, coloquei igual apenas para ficar mais facil de entender.
        Agora, notem que, após o virgula, eu estou passando 4 req.bodys, por que ? Oras, vejam bem, eu passei 4 pontos de interrogações. Ou seja, pontos de interrogações servem para
          definirmos a quantidade de informações que serao passadas após o virgula, e o que for apos esse virgula serao os valores que foram enviados no momento da requisição, entao aproveitaremos o embalo para pegar seus valores
</ul>

<img src="https://gitlab.com/leandrost15/api---agricultura-familiar-organica/raw/master/Capturar.PNG" />

Exemplo de inserção sem a passagem de argumentos;

database.query('insert into Usuarios(nome,email,login,senha) values (Luquinha,luskinha@hotamil.gov,luskaoOPerigo,luska123'),function(error,results,fields){
